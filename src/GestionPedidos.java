import java.util.Date;
import java.util.Scanner;

/**
 * Clase GestionPedidos
 * En esta clase se gestionan los pedidos haciendo uso del resto de clases y objetos creados
 * @author Sebas
 * @version 1.0
 */
public class GestionPedidos {
    /**
     * Variable global que corresponde al código que se asignará a un pedido.
     * Con cada pedido nuevo se incremente en 1 para que así ningún pedido tenga
     * el mismo código
     */
    static int codigoPedido = 1;

    //Métodos

    /**
     * Muestra el menú inicial donde se puede escoger entre crear clientes o pedidos
     */
    public static void menuInicial(){
        System.out.println("""
                ### MENÚ INICIAL ###
                1. Crear clientes
                2. Crear productos""");
    }

    /**
     * Muestra el menú pedidos donde se puede escoger realizar un pedido o salir del programa
     */
    public static void menuPedidos(){
        System.out.println("""
                ### MENÚ PEDIDOS ###
                1.Realizar pedidos
                2.Salir"""
        );
    }

    /**
     * Pide al usuario que introduzca los datos de un nuevo cliente
     * @param cliente Cliente al que se le psarán los datos pedidos
     */
    public static void pedirDatosClientes(Cliente cliente){
        Scanner sc = new Scanner(System.in);
        String nombre;
        String apellidos;
        int telefono;
        String direccion;
        int n;
        boolean error;
        System.out.print("Nombre del cliente: ");
        nombre = sc.nextLine();
        System.out.print("Apellidos del cliente: ");
        apellidos = sc.nextLine();
        do {
            System.out.print("Teléfono: ");
            telefono = sc.nextInt();
            n = telefono/100000000;
            if (n != 6 && n != 7 && n != 8 && n != 9){
                System.out.println("Número incorrecto, introducelo de nuevo");
                error = true;
            }
            else{
                error = false;
            }
        } while(error);
        sc.nextLine();
        System.out.print("Dirección de cliente: ");
        direccion = sc.nextLine();
        cliente.nombre = nombre;
        cliente.apellidos = apellidos;
        cliente.fechaDeALta = new Date();
        cliente.telefono = telefono;
        cliente.direccion = direccion;
    }

    /**
     * Menú que muestra los distintos productos creados para hacer un pedido
     * @param p1 producto1
     * @param p2 producto2
     * @param p3 producto3
     * @param p4 producto4
     * @param p5 producto5
     */
    public static void menuProductos(Producto p1, Producto p2, Producto p3,
                                     Producto p4, Producto p5){
        System.out.println("### SELECCIONE EL PRODUCTO QUE DESEA ###\n" +
                "1." + p1.nombre + "\n" +
                "2." + p2.nombre + "\n" +
                "3." + p3.nombre + "\n" +
                "4." + p4.nombre + "\n" +
                "5." + p5.nombre + "\n"
        );
    }

    /**
     * Pide los datos de un producto y los asigna al producto pasado por parámetro
     * @param producto Clase Producto al que le asignamos los datos leídos
     */
    public static void pedirDatosProductos(Producto producto){
        Scanner sc = new Scanner(System.in);
        String nombre;
        float precio;
        System.out.print("Nombre del producto: ");
        nombre = sc.nextLine();
        System.out.print("Precio del producto: ");
        precio = sc.nextFloat();
        producto.nombre = nombre;
        producto.precio = precio;
    }

    /**
     * Método donde se realiza un pedido y al que le pasamos como parámetros
     * los productos y clientes creados previamente
     * @param c1 Cliente1
     * @param c2 Cliente2
     * @param c3 Cliente3
     * @param p1 Producto1
     * @param p2 Producto2
     * @param p3 Producto3
     * @param p4 Producto4
     * @param p5 Producto5
     */
    public static void realizarPedido(Cliente c1, Cliente c2, Cliente c3,
                                      Producto p1, Producto p2, Producto p3,
                                      Producto p4, Producto p5){
        Scanner sc = new Scanner(System.in);
        Pedido pedido = new Pedido();
        int cliente = 0;
        int option;
        int cantidad;
        int productosMax = 1;
        int tel;
        boolean errorCliente = true;
        char respuestaMetodoPago;
        //Se pide el numero del cliente y se comprueba que exista
        do {
            System.out.print("Introduce el teléfono del cliente: ");
            tel = sc.nextInt();
            if(tel == c1.telefono){
                pedido.agregarCliente(c1);
                errorCliente = false;
                cliente = 1;
            }
            else if(tel == c2.telefono){
                pedido.agregarCliente(c2);
                errorCliente = false;
                cliente = 2;
            }
            else if(tel == c3.telefono){
                pedido.agregarCliente(c3);
                errorCliente = false;
                cliente = 3;
            }
            else{
                System.out.println("Número equivocado.");
            }
        }while(errorCliente);
        //se hace el pedido
        do {
            menuProductos(p1, p2, p3, p4, p5);
            option = sc.nextInt();
            sc.nextLine();
            switch (option) {
                case 1 -> {
                    System.out.print("¿Cuantas unidades del producto desea?: ");
                    cantidad = sc.nextInt();
                    sc.nextLine();
                    if (productosMax == 1) {
                        p1.cantidad = cantidad;
                        pedido.p1 = p1;
                    } else if (productosMax == 2) {
                        p1.cantidad = cantidad;
                        pedido.p2 = p1;
                    }
                    productosMax++;
                }
                case 2 -> {
                    System.out.print("¿Cuantas unidades del producto desea?: ");
                    cantidad = sc.nextInt();
                    sc.nextLine();
                    if (productosMax == 1) {
                        p2.cantidad = cantidad;
                        pedido.p1 = p2;
                    } else if (productosMax == 2) {
                        p2.cantidad = cantidad;
                        pedido.p2 = p2;
                    }
                    productosMax++;
                }
                case 3 -> {
                    System.out.print("¿Cuantas unidades del producto desea?: ");
                    cantidad = sc.nextInt();
                    sc.nextLine();
                    if (productosMax == 1) {
                        p3.cantidad = cantidad;
                        pedido.p1 = p3;
                    } else if (productosMax == 2) {
                        p3.cantidad = cantidad;
                        pedido.p2 = p3;
                    }
                    productosMax++;
                }
                case 4 -> {
                    System.out.print("¿Cuantas unidades del producto desea?: ");
                    cantidad = sc.nextInt();
                    sc.nextLine();
                    if (productosMax == 1) {
                        p4.cantidad = cantidad;
                        pedido.p1 = p4;
                    } else if (productosMax == 2) {
                        p4.cantidad = cantidad;
                        pedido.p2 = p4;
                    }
                    productosMax++;
                }
                case 5 -> {
                    System.out.print("¿Cuantas unidades del producto desea?: ");
                    cantidad = sc.nextInt();
                    sc.nextLine();
                    if (productosMax == 1) {
                        p5.cantidad = cantidad;
                        pedido.p1 = p5;
                    } else if (productosMax == 2) {
                        p5.cantidad = cantidad;
                        pedido.p2 = p5;
                    }
                    productosMax++;
                }
                default -> System.out.println("Opción errónea, inténtelo de nuevo");
            }
        } while (option != 2 && productosMax < 3);
        pedido.codigoPedido = codigoPedido;
        codigoPedido++;
        pedido.toString();
        System.out.print("¿Quieres realizar el pago en efectivo(1), tarjeta(2) o cuenta bancaria(3)?: ");
        respuestaMetodoPago = sc.next().charAt(0);
        PasarelaDePago pago = new PasarelaDePago();
        switch (respuestaMetodoPago) {
            case '1' -> pago.efectivo();
            case '2' -> pago.tarjeta();
            case '3' -> pago.cuenta();
            default -> System.out.println("ERROR");
        }
        switch (cliente) {
            case 1 -> c1.agregarPedido(pedido);
            case 2 -> c2.agregarPedido(pedido);
            case 3 -> c3.agregarPedido(pedido);
        }
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int option;
        int contador = 1;
        Cliente cliente1 = new Cliente();
        Cliente cliente2 = new Cliente();
        Cliente cliente3 = new Cliente();
        Producto p1 = new Producto();
        Producto p2 = new Producto();
        Producto p3 = new Producto();
        Producto p4 = new Producto();
        Producto p5 = new Producto();
        while (contador < 3) {
            do {
                menuInicial();
                option = sc.nextInt();
                switch (option) {
                    case 1 -> {
                        pedirDatosClientes(cliente1);
                        pedirDatosClientes(cliente2);
                        pedirDatosClientes(cliente3);
                    }
                    case 2 -> {
                        pedirDatosProductos(p1);
                        pedirDatosProductos(p2);
                        pedirDatosProductos(p3);
                        pedirDatosProductos(p4);
                        pedirDatosProductos(p5);
                    }
                    default -> System.out.println("Opción incorrecta, vuelva a intentarlo");
                }
            } while (option != 1 && option != 2);
            contador++;
        }
        do {
            menuPedidos();
            option = sc.nextInt();
            switch (option){
                case 1: realizarPedido(cliente1, cliente2, cliente3, p1, p2,
                        p3, p4, p5);
                break;
                case 2: break;
                default: System.out.println("Opción errónea, inténtelo de nuevo");
                    break;
            }
        } while (option != 2);
    }
}
