/**
 * Clase Pedido
 * Contiene los datos de un pedido y sus métodos correspondientes
 * @author Sebas
 * @version 1.0
 */
public class Pedido {
    /**
     * Cliente asociado al pedido
     */
    Cliente cliente;
    /**
     * Producto 1 del pedido
     */
    Producto p1;
    /**
     * Producto 2 del pedido
     */
    Producto p2;
    /**
     * Importe total del pedido
     */
    float importeTotal;
    /**
     * Código del pedido, es un número que no se repite
     */
    int codigoPedido;

    /**
     * Enum estado que representa los posibles estados del pedido(no se utiliza)
     */
    enum estado{
        Pagado, Preparando, Listo, Servido
    }
    estado estado;
    //Contructores
    Pedido(){}
    //Métodos

    /**
     * Asocia un cliente al pedido
     * @param cliente Clase Cliente que se pasa al atributo cliente
     */
    void agregarCliente(Cliente cliente){
        this.cliente = cliente;
    }

    /**
     * Agrega un producto al pedido
     * @param p1 Clase Producto que se guarda en el atributo producto1
     */
    void agregarProducto1(Producto p1){
        this.p1 = p1;
    }

    /**
     * Agrega un producto al pedido
     * @param p2 Clase Producto que se guarda en el atributo producto2
     */
    void agregarProducto2(Producto p2){
        this.p2 = p2;
    }

    /**
     * Elimina un producto(no se utiliza)
     * @param producto
     */
    void eliminarProducto(Producto producto){}

    /**
     * Muestra un resumen del pedido como un recibo
     * @return
     */
    @Override
    public String toString() {
        importeTotal = (this.p1.cantidad*this.p1.precio + this.p2.cantidad*this.p2.precio);
        System.out.println("""
            CANT.   PRODUCTO    PRECIO UD   TOTAL
            =====   ========    =========   =====\n""" +
                this.p1.cantidad + "    " + this.p1.nombre + "      " + this.p1.precio + "      " + (this.p1.cantidad*this.p1.precio) + "\n" +
                this.p2.cantidad + "    " + this.p2.nombre + "      " + this.p2.precio + "      " + (this.p2.cantidad*this.p2.precio) + "\n" +
                "TOTAL-----------------------> " + importeTotal + "€"
        );
        return "Pedido{}";
    }

}
