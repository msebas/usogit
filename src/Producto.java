/**
 * Clase Producto
 * Contiene los datos de un producto
 * @author Sebas
 * @version 1.0
 */
public class Producto {
    /**
     * Nombre del producto
     */
    String nombre;
    /**
     * Precio del producto
     */
    float precio;
    /**
     * Cantidad del producto
     */
    int cantidad;
    //Constructores
    public Producto() {}
}
