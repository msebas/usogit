import java.util.Date;

/**
 * Clase PasarelaDePago que en un principio gestionaba los procesos de pago de un pedido
 * @author Sebas
 * @version 1.0
 */
public class PasarelaDePago {
    /**
     * Importe del pedido
     */
    int importe;
    /**
     * Código del pago generado con getTime() (Al final no se utiliza)
     */
    Date codigoPago;
    //Constructores
    /*
    Métodos que gestionaban en un principio los distintos métodos de pago:
    efectivo, tarjeta y transferencia bancaria
     */

    /**
     * Gestiona el pago en efectivo
     */
    public void efectivo(){
        System.out.println("Has pagado en efectivo");
    }

    /**
     * Gestiona el pago con tarjeta
     */
    public void tarjeta(){
        System.out.println("Has pagado con tarjeta");
    }

    /**
     * Gestiona el pago mediante transferencia bancaria
     */
    public void cuenta(){
        System.out.println("Has pagado con cuenta bancaria");
    }
}
