import java.util.Date;
import java.util.Objects;

/**
 * Clase CLiente
 * Contine información de cada cliente
 * @author Sebas
 * @version 1.0
 */
public class Cliente {
    //Atributos
    /**
     * Nombre del cliente
     */
    String nombre;
    /**
     * Apellidos del cliente
     */
    String apellidos;
    /**
     * Fecha de alta del cliente
     */
    Date fechaDeALta;
    /**
     * Teléfono del cliente
     */
    int telefono;
    /**
     * Dirección del cliente
     */
    String direccion;
    /**
     * Historial de pedidos del cliente
     */
    String historial;
    //Constructores
    Cliente(){}
    //Métodos públicos

    /**
     * Añade el código de un nuevo pedido al historial del cliente
     * @param pedido clase Pedido del cual obtenemos su código
     */
    void agregarPedido(Pedido pedido){
        String cod;
        cod = pedido.codigoPedido+"";
        if (Objects.equals(historial, "")) {
            historial = "Pedido " + cod;
        }
        else{
            historial = historial + "\nPedido " + cod;
        }
    }
}
